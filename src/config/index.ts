import { bold } from 'chalk';
import { customLog, sequencialColor } from 'termx';

export const log = customLog(sequencialColor(), bold('CAMION'));