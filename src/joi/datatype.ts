export default {
    dataToString: (data:any) => {
      const type = typeof data;
      let str = '';
  
      switch (type) {
        case 'number': case 'bigint': case 'boolean':
          str = data.toString();
          break;
        case 'object':
          str = JSON.stringify(data);
          break;
        case 'string': default:
          break;
      }
  
      return str;
    },
};
  