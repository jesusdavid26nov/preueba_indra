import { Request, Response, NextFunction } from "express";
import joiErrors from './joi.error';
export default (req:Request, res:Response, next:NextFunction) => {
  try {  
    const url : any = req.url;
    const schemaPath = `../api${req.baseUrl}/schema`;
    const routeSchemas = require(schemaPath);

    const pathSchema = `${req.method.toLowerCase()}_${url.match(/(?:\/)([a-zA-Z_]{1,})(?:\??)/)[1]}`;
     
    if (routeSchemas[pathSchema].headers) {
      const headersValidation = routeSchemas[pathSchema].headers.validate(req.headers);
      if (headersValidation.error) { throw Object.values(joiErrors.invalidFields(headersValidation.error, 'headers')) }
    }

    if (routeSchemas[pathSchema].params) {
      const paramsValidation = routeSchemas[pathSchema].params.validate(req.params);
      if (paramsValidation.error) { throw Object.values(joiErrors.invalidFields(paramsValidation.error, 'params')) }
    }

    if (routeSchemas[pathSchema].query) {
      const queryValidation = routeSchemas[pathSchema].query.validate(req.query);
      if (queryValidation.error) { throw Object.values(joiErrors.invalidFields(queryValidation.error, 'query')) }
    } else if (Object.entries(req.query).length) {
      throw Object.values(joiErrors.unevaluatedFields('query'));
    }

    if (routeSchemas[pathSchema].body) {
      const bodyValidation = routeSchemas[pathSchema].body.validate(req.body);
      if (bodyValidation.error) { throw Object.values(joiErrors.invalidFields(bodyValidation.error, 'body')) }
    } else if (Object.entries(req.body).length) {
      throw Object.values(joiErrors.unevaluatedFields('body'));
    }
    next();
    return {};
  } catch (error) {
    return next(error);
  }
};
