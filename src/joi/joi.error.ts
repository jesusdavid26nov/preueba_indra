import datatype  from './datatype';

export default {
  unevaluatedFields: (type:any) => {
    const field = type && typeof (type) === 'string' ? type.toUpperCase() : datatype.dataToString(type).toUpperCase();

    return {
      code    : `UNVALIDATED_${field}_CONTENT`,
      message : `Incoming <${field}> data in not being validated`,
      handled : true,
    };
  },

  invalidFields: (error:any, type:any) => {
    const field = type && typeof (type) === 'string' ? type.toUpperCase() : datatype.dataToString(type).toUpperCase();

    return {
      code    : `INVALID_${field}_VALUE_CONTENT`,
      message : error && error.message ? error.message : 'The request have unvalidated data',
      handled : true,
    };
  },
};
