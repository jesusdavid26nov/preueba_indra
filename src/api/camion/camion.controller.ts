import { Request, Response } from "express";

export default {

    camion: async(req:Request, res:Response) =>{
        try {
            let { packageIntro } = req.body;
            let duplicate: any[] = [];
            const packages: any[] = [10,60,40,35,20];
     
            const tempArray = [...packageIntro].sort();
            
            for (let i = 0; i < tempArray.length; i++) {
                if (tempArray[i + 1] === tempArray[i]) {
                    duplicate.push(tempArray[i]);
                }
            }

            if (duplicate.length){
               return res.json({ message: 'valores duplicados'})
            }
            
            let suma: any = 0;
            let errorPackage :any = []
            packageIntro.forEach ((packageIntro:any) =>{
                if (packages.includes(packageIntro)) {
                    suma += packageIntro;

                }else{
                    errorPackage.push(packageIntro)
                }
            })

            if(suma == 60){
                res.json({ message: ' la suma de los paquete es de 60'})
            }else {
                res.json({ message: `la suma del packete no es la adecuada ${errorPackage}`})
            }
            
        } catch (error) {
            
        }
        
    }
}
