import { Router } from "express";

import _camion from "./camion/route";
import _helado from "./helado/route";
const index = Router();

index.use('/camion',  _camion);
index.use('/helado',  _helado);




export default index;