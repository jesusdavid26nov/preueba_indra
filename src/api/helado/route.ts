import { Router } from "express";

import controller from "./helador.controller";
import valid from "../../joi/joi";

const _user =  Router();

_user.post('/create', valid, controller.helado);



export default _user;