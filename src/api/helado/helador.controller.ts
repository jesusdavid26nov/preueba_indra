import { Request, Response } from "express";

export default {

    helado: async(req:Request, res:Response) =>{
      const { pay } = req.body;
      const cash = [5,10,20]
      let errorPAY: any = []
      let subtotal : any = 0
      pay.forEach ((pay:any) =>{
        if (cash.includes(pay)) {
            subtotal += pay

        }else{
            errorPAY.push(pay)
        }

      });

      
      res.json({
          total: { message: `se puede devolver el cambio  S ${subtotal}`},
          error: { error: `billete no aceptado ${errorPAY}`}
      })
    }
}