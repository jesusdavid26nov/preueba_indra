import express from "express";
import morgan from "morgan";
import cors from 'cors';
import index from "./api/index";
import { log } from "./config";




const app = express();


app
    //middleware
    .use(express.json({limit:'5mb'}))
    .use(morgan('dev'))
    .use(cors())
    .use(express.urlencoded({ extended: true }))
    .use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
        return next();
    })
      
    //routes links
    .use('/',index)



export default app;